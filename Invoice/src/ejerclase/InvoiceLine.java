package ejerclase;

import java.text.DecimalFormat;

public class InvoiceLine {

	private int qty;
	private String item;
	private String description;
	private float unitPrice;
	private float discount;
	private float lineTotal;
	public static final String LINE_HEADER = "-----------------------------------------------------------------------------------------------------";
	public static final String FORMAT_STRING = "| %9s | %10s | %30s | %13s | %8s | %13s|";
	public static final String HEADER = LINE_HEADER + "\n"
			+ String.format(FORMAT_STRING, "QTY", "ITEM #", "DESCRIPTION", "UNIT PRICE", "DISCOUNT", "LINE TOTAL")
			+ "\n" + LINE_HEADER;

	public InvoiceLine(int qty, String item, String description, float unitPrice, float discount) {
		this.qty = qty;
		this.item = item;
		this.description = description;
		this.unitPrice = unitPrice;
		this.discount = discount;
		lineTotal = 0;
	}

	public String toString() {
		String s = String.format("| %9s | %10s | %-30s | %13s | %7s%% | %13s|", customFormat("###,###", qty), item,
				description, customFormat("$##,###.00", unitPrice), customFormat("#0.00", discount),
				customFormat("$##,###.00", calculateLineTotal()));
		return s;

	}

	public float calculateLineTotal() {
		lineTotal = (qty * unitPrice) - ((qty * unitPrice) * (discount / 100));
		return lineTotal;
	}

	public float getTotalLine() {
		return lineTotal;
	}

	public static String customFormat(String pattern, float value) {
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		String output = myFormatter.format(value);
		return output;
	}
}
