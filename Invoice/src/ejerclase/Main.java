package ejerclase;

import java.util.*;

public class Main {

	public static void main(String[] args) {
		InvoiceLine i = new InvoiceLine(1, "YMIG", "Your Mom Is Gay", 1000, 0);
		InvoiceLine i2 = new InvoiceLine(1, "YMIG", "Your Mom Is Gay", 1000, 0);
		InvoiceLine i3 = new InvoiceLine(1, "YMIG", "Your Mom Is Gay", 1000, 0);
		InvoiceLine i4 = new InvoiceLine(1, "YMIG", "Your Mom Is Gay", 1000, 0);

		Invoice in = new Invoice("a", createLargeDate(2015, 5, 10), "a", "a", "a", "a", "a", createDate(2019, 11, 29), "a",
				createDate(2015, 5, 10), 5, i, i2, i3, i4);
		System.out.println(in.toString());
	}

	public static Date createDate(int year, int month, int day) {
		Calendar myCalendar = new GregorianCalendar(year, month - 1, day);
		Date myDate = myCalendar.getTime();
		return myDate;
	}
	
	public static Date createLargeDate(int year, int month, int day) {
		Calendar myCalendar = new GregorianCalendar(year, month - 1, day);
		Date myDate = myCalendar.getTime();
		return myDate;
	}

}
