package ejerclase;

import java.text.*;
import java.util.*;

public class Invoice {

	private InvoiceLine[] lines;
	private String invoice_number;
	private Date date;
	private String to;
	private String ship_to;
	private String salesPerson;
	private String shipingMethod;
	private String shipingTerms;
	private Date deliveryDate;
	private String paymentTerms;
	private Date dueDate;
	private float totalDiscount;

	public static final String LINE_HEADER = "-----------------------------------------------------------------------------------------------------";
	public static final String LINE_DISCOUNT = "									  ---------------------------";
	public static final String LINE_FINAL = "									  	     ----------------";
	public static final String FORMAT_STRING = "| %18s | %14s | %13s | %13s | %13s | %12s|";
	public static final String FORMAT_DISCOUNT_STRING = " %72s |  %6s%% | %13s|";
	public static final String FORMAT_FINAL_STRING = " %83s | %13s|";
	public static final String HEADER = LINE_HEADER + "\n" + String.format(FORMAT_STRING, "SALES PERSON",
			"SHIPING METHOD", "SHIPING TERMS", "DELIVERY DATE", "PAYMENT TERMS", "DUE DATE") + "\n" + LINE_HEADER
			+ "\n";

	public Invoice(String invoice_number, Date date, String to, String ship_to, String salesPerson,
			String shipingMethod, String shipingTerms, Date deliveryDate, String paymentTerms, Date dueDate,
			float totalDiscount, InvoiceLine... lines) {
		this.invoice_number = invoice_number;
		this.date = date;
		this.to = to;
		this.ship_to = ship_to;
		this.salesPerson = salesPerson;
		this.shipingMethod = shipingMethod;
		this.shipingTerms = shipingTerms;
		this.deliveryDate = deliveryDate;
		this.paymentTerms = paymentTerms;
		this.dueDate = dueDate;
		this.totalDiscount = totalDiscount;
		this.lines = lines;

	}

	public String printNumAndDate() {
		return invoice_number + "\n" + normalLargeDate(date);
	}

	public String printTo() {
		return to;
	}

	public String printShipTo() {
		return ship_to;
	}

	public String printLineInformation() {
		String s = HEADER;
		s += String.format(FORMAT_STRING, salesPerson, shipingMethod, shipingTerms, normalDate(deliveryDate),
				paymentTerms, normalDate(dueDate)) + "\n";
		s += LINE_HEADER;
		return s;
	}

	@Override
	public String toString() {
		String s = "";
		s = printNumAndDate() + "\n" + printTo() + "\n" + printShipTo() + "\n\n\n\n" + printLineInformation()
				+ "\n\n\n\n";
		s += InvoiceLine.HEADER + "\n";
		for (int i = 0; i < lines.length; i++) {
			s += lines[i] + "\n";
		}
		s += LINE_HEADER + "\n";
		s += formatDiscount() + "\n" + LINE_DISCOUNT + "\n" + formatSubtotal() + "\n" + LINE_FINAL + "\n" + formatVAT()
				+ "\n" + LINE_FINAL + "\n" + formatTotal() + "\n" + LINE_FINAL + "\n";
		return s;
	}

	public String normalDate(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
		String date = sdf.format(d);
		return date;
	}
	
	public String normalLargeDate(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM, yyyy");
		String date = sdf.format(d);
		return date;
	}

	public float calculateTotalPrice() {
		float invoiceTotalPrice = 0;
		for (int i = 0; i < lines.length; i++) {
			invoiceTotalPrice += lines[i].getTotalLine();
		}
		return invoiceTotalPrice;
	}

	public float calculateTotalDiscount() {
		return calculateTotalPrice() * (totalDiscount / 100);
	}

	public float calculateSubtotal() {
		return calculateTotalPrice() - calculateTotalDiscount();
	}

	public float calculateVAT() {
		return ((calculateSubtotal() * 21) / 100);
	}

	public float calculateTotal() {
		return calculateSubtotal() + calculateVAT();
	}

	public String formatDiscount() {
		String s = "";
		s += String.format(FORMAT_DISCOUNT_STRING, "TOTAL DISCOUNT", totalDiscount,
				customFormat("$##,###.00", calculateTotalDiscount()));
		return s;
	}

	public String formatSubtotal() {
		String s = "";
		s += String.format(FORMAT_FINAL_STRING, "SUBTOTAL", customFormat("$##,###.00", calculateSubtotal()));
		return s;
	}

	public String formatVAT() {
		String s = "";
		s += String.format(FORMAT_FINAL_STRING, "VAT", customFormat("$##,###.00", calculateVAT()));
		return s;
	}

	public String formatTotal() {
		String s = "";
		s += String.format(FORMAT_FINAL_STRING, "TOTAL", customFormat("$##,###.00", calculateTotal()));
		return s;
	}

	public static String customFormat(String pattern, float value) {
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		String output = myFormatter.format(value);
		return output;
	}

}
